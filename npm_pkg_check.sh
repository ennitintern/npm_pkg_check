#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-r] -p path_to_search package1 [package2...]

Available options:

-h, --help          Print this help and exit
-v, --verbose       Print script debug info
-r, --recursive     Search recursively through the path
-p, --path          Path to search
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  recursive=0
  path=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    -r | --recursive) recursive=1 ;; 
    -p | --path)
      path="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  [[ -z "${path-}" ]] && die "Missing required parameter: path"
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}

parse_params "$@"
setup_colors

msg "${RED}Read parameters:${NOFORMAT}"
msg "- recursive: ${recursive}"
msg "- path: ${path}"
msg "- packages: ${args[*]-}"

msg "${GREEN}Start checking for packages...${NOFORMAT}"
cd ${path} 

if (( $recursive == 1 ))
then
    for d in ${path}/*
    do
        cd "$d"
        for ((i=0; i<${#args[@]}; i++))
        do
            npm ls ${args[i]} || true
        done
    done
else
    for ((i=0; i<${#args[@]}; i++))
    do
        npm ls ${args[i]}
    done
fi
msg "${GREEN}Done${NOFORMAT}"
