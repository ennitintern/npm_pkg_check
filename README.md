```
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-r] -p path_to_search package1 [package2...]
```

### Available options:

```
-h, --help          Print this help and exit
-v, --verbose       Print script debug info
-r, --recursive     Search recursively through the path
-p, --path          Path to search
```

### To run the script over a parent directory with multiple projects:

```
./npm_pkg_check.sh -r -p /srv/www/ rc coa
```

### To run the script over one Project:

```
./npm_pkg_check.sh -p /srv/www/awesome_project rc coa
```
